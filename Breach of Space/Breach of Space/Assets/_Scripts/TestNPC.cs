﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class TestNPC : MonoBehaviour
{
    float reach;
    public GameObject halo;

    public bool selected;
    public bool working;
    public bool moving;
    List<GameObject> cleaning = new List<GameObject>();
    Vector3 destination;

    void Update()
    {
        if (moving)
        {

        }
        if (!selected)
        {
            halo.GetComponent<Light>().range = 0;
        }
        else
        {
            halo.GetComponent<Light>().range = 10;
        }
    }

    public void JobAssignment(GameObject obj, float ox, float oz)
    {
        StopAllCoroutines();
        foreach(GameObject g in cleaning)
        {
            Destroy(g);
        }
        working = false;
        moving = true;

        if(obj != null)
        {
            Debug.Log(obj.tag + ": TEST NPC OBJ");
            switch (obj.tag)
            {
                case "TestMachine":
                    obj.GetComponent<TestMachineScript>().MakeTheDonuts(this.gameObject);
                    reach = 1f;
                    break;
                case "Pneumatic":
                    obj.GetComponent<TestPneumatic>().selected = true;
                    reach = 0.1f;
                    break;
                default:
                    Debug.LogError("No such item exists.");
                    break;
            }
            obj.GetComponent<Light>().range = 10f;
            StartCoroutine(WorkinHard(new Vector3(ox, .625f, oz)));
        }
        else
        {
            StartCoroutine(HardlyWorkin(new Vector3(ox, .625f, oz)));
        }
    }

    IEnumerator WorkinHard(Vector3 v)
    {
        Debug.Log("WORKIN' HARD...");
        while ((!working) &&
            Vector3.Distance(transform.position, v) > reach)
        {
            Debug.Log("if !working");
            transform.position = Vector3.MoveTowards(transform.position, v, 0.025f);
            yield return new WaitForEndOfFrame();
        }
        moving = false;
        Debug.Log("end of workin' hard");
        selected = false;
        yield return null;
    }

    IEnumerator HardlyWorkin(Vector3 v)
    {
        Debug.Log("... OR HARDLY WORKIN?");
        GameObject lite = new GameObject("Destination");
        lite.AddComponent<Light>();
        lite.transform.position = v;
        cleaning.Add(lite);
        while(Vector3.Distance(transform.position, v) > 0.125f)
        {
            Debug.Log("inside movement of hardlyworkin");
            transform.position = Vector3.MoveTowards(transform.position, v, 0.025f);
            yield return new WaitForEndOfFrame();
        }
        moving = false;
        Destroy(lite);
        selected = false;
        yield return null;
    }
}
