﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class TestClickerV3 : MonoBehaviour
{
    public Camera mainCam;
    public Camera aCam;
    public Camera bCam;
    public Camera cCam;


    Camera portalCam = null;
    GameObject currentStaff;
    GameObject currentTask;

    // multiple monitors
    // (there has to be a better way!)
    public GameObject screen3B;
    public GameObject screen3C;

    // bools for Cinemachine
    public bool viewL;
    public bool viewR;
    public bool computing;
    public bool vending;

    void Update()
    {
        
        GetComponent<Animator>().SetBool("viewL", viewL);
        GetComponent<Animator>().SetBool("viewR", viewR);
        GetComponent<Animator>().SetBool("vending", vending);
        GetComponent<Animator>().SetBool("computing", computing);

        if (!LevelHandlerTest.gameOver && LevelHandlerTest.gameOn)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (portalCam == null)
                {
                    RaycastHit theHit;
                    Ray camRay = mainCam.ScreenPointToRay(Input.mousePosition);

                    if (Physics.Raycast(camRay, out theHit))
                    {
                        Debug.Log(theHit.collider.name);
                        switch (theHit.collider.name)
                        {
                            case "Screen 3A":
                                if (portalCam != aCam)
                                {
                                    currentStaff = null;
                                    portalCam = aCam;
                                    CameraShifter(2);
                                }
                                break;
                            case "Screen 3B":
                                if (portalCam != bCam)
                                {
                                    currentStaff = null;
                                    portalCam = bCam;
                                    CameraShifter(3);
                                }
                                break;
                            case "Screen 3C":
                                if (portalCam != cCam)
                                {
                                    currentStaff = null;
                                    portalCam = cCam;
                                    CameraShifter(3);
                                }
                                break;
                            default:
                                portalCam = null;
                                CameraShifter(0);
                                break;
                        }
                    }
                }
                else
                {
                    // if portalCam != null...
                    //    - if an NPC is not selected...
                    //         - create a Raycast Array to see if
                    //           we just cannot see the NPC, stopping
                    //           the loop when we hit an NPC or a Wall
                    //           (to keep from selecting NPCs in other
                    //           departments). If we find an NPC, that
                    //           NPC is the currentStaff.
                    //    - else
                    //         - create ONE ray
                    //         - if we click an NPC
                    //             - that NPC is selected
                    //         - if we click on a task
                    //             - that NPC is assigned a task
                    //         - otherwise
                    //             - that NPC goes to the point clicked.

                    RaycastHit theHit;
                    Ray camRay = mainCam.ScreenPointToRay(Input.mousePosition);

                    if (Physics.Raycast(camRay, out theHit))
                    {
                        Debug.Log(theHit.collider.name);
                        if (currentStaff == null)
                        {
                            Ray outRay = portalCam.ViewportPointToRay(theHit.textureCoord);
                            RaycastHit[] xray = Physics.RaycastAll(outRay);

                            foreach (RaycastHit h in xray)
                            {
                                Debug.Log(h.collider.gameObject.name);
                                if (h.collider.gameObject.tag == "NPC")
                                {
                                    Debug.Log("AN NPC!");
                                    currentStaff = h.collider.gameObject;
                                    currentStaff.GetComponent<TestNPCv2>().selected = true;
                                    return;
                                    // stops search
                                }
                                else if (h.collider.gameObject.CompareTag("Walls"))
                                {
                                    Debug.Log("WALL");
                                    return;
                                    // stops search
                                }
                                else
                                {
                                    Debug.Log("Nothing!");
                                    // keep searching!
                                }
                            }

                        }
                        else
                        {
                            Ray outRay = portalCam.ViewportPointToRay(theHit.textureCoord);
                            RaycastHit outHit;

                            if (Physics.Raycast(outRay, out outHit))
                            {
                                Debug.Log(outHit.collider.tag);
                                if (outHit.collider.tag == "NPC")
                                {
                                    currentStaff = outHit.collider.gameObject;
                                    currentStaff.GetComponent<TestNPCv2>().selected = true;
                                }
                                else if (outHit.collider.tag == "Task")
                                {
                                    Debug.Log("HEY, A TASK!");
                                    // task stuff!
                                    outHit.collider.GetComponent<TaskmasterTest>().selectedNPC = currentStaff;
                                    currentStaff.GetComponent<TestNPCv2>().
                                        WorkinHard(outHit.collider.GetComponent<TaskmasterTest>());
                                    currentStaff = null;
                                }
                                else
                                {
                                    Debug.Log("ELSE...?");
                                    // must be a location
                                    currentStaff.GetComponent<TestNPCv2>().HardlyWorkin(
                                        outHit.point);
                                    currentStaff = null;
                                }
                            }
                            else
                            {
                                Debug.Log("I did not see anything?");
                            }
                        }
                    }
                }
            }
            if (Input.GetKeyDown(KeyCode.LeftShift) ||
                Input.GetKeyDown(KeyCode.RightShift))
            {
                if (portalCam == null)
                {

                    if (!vending && !computing)
                    {
                        CameraShifter(1);
                    }
                    else
                    {
                        if (computing)
                        {
                            CameraShifter(4);
                        }
                        else
                        {
                            CameraShifter(0);
                        }
                    }

                }
                else
                {
                    switch (portalCam.name)
                    {
                        case "CAM A3":
                            break;
                        case "CAM B3":
                            screen3C.SetActive(true);
                            screen3B.SetActive(false);

                            portalCam = cCam;
                            break;
                        case "CAM C3":
                            screen3B.SetActive(true);
                            screen3C.SetActive(false);

                            portalCam = bCam;
                            break;
                        default:
                            Debug.LogError("No such camera.");
                            break;
                    }
                }
            }
            /*
            if (Input.GetKeyDown(KeyCode.RightShift))
            {
                if (portalCam == null)
                {
                    if (vending)
                    {
                        CameraShifter(0);
                    }
                    else
                    {
                        CameraShifter(4);
                    }
                }
                else
                {
                    // switch camera view to
                    //        currentView +1
                }
            }
            */
            if (Input.GetKeyDown(KeyCode.Space))
            {
                portalCam = null;
                CameraShifter(0);
            }
        }
    }

    public void CameraShifter(int i)
    {
        //Debug.Log(i);
        switch (i)
        {
            case 0:
                viewR = false;
                viewL = false;
                vending = false;
                computing = false;
                break;
            case 1:
                viewR = false;
                viewL = false;
                vending = false;
                computing = true;
                break;
            case 2:
                viewR = false;
                viewL = true;
                vending = false;
                computing = false;
                break;
            case 3:
                viewR = true;
                viewL = false;
                vending = false;
                computing = false;
                break;
            case 4:
                viewR = false;
                viewL = false;
                computing = false;
                vending = true;      
                break;
            default:
                Debug.LogError("No such camera exists...");
                viewR = false;
                viewL = false;
                vending = false;
                computing = false;
                break;
        }
    }

    /*
    IEnumerator CamMovement(Vector3 p, Vector3 r)
    {
        // p = position
        // r = rotation
        p.z -= 3f; // adjusts so the camera can see the screen.

        // FIX THIS ---------------------------------------------------------
        // CURRENTLY DOES NOT DO WHAT YOU WANT ------------------------------
        // NOR DOES HITTING SPACEBAR? ---------------------------------------

        if(Vector3.Distance(p, transform.position) > 0)
        {
            transform.position = Vector3.Lerp(p, transform.position, .0002f);
            if(Vector3.Distance(transform.rotation.eulerAngles, r) > 0)
            {
                transform.Rotate(r);
            }
            Debug.Log(transform.position);
            yield return new WaitForSeconds(5f);
        }
        moving = false;
        yield return null;
    }
    */
}
