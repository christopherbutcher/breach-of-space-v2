﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskmasterTest : MonoBehaviour
{
    public GameObject selectedNPC;

    public Task task;
    // This is the current Task on this
    //      object. All data and special
    //      methods will come from the
    //      Scriptable Object. The Break()
    //      and Fix() methods will use a
    //      switch to determine what must
    //      be called from the S.O.

    float timer;
    // Timer for the break function, set to
    //       a random # whose range is deter-
    //       mined by the Task Scriptable 
    //       Object above.

    public static List<Task>  GAS_TASKS = new List<Task>() { };
    public static List<Task> TECH_TASKS = new List<Task>() { };
    // These are used to keep track of
    //       all of the tasks of certain
    //       types for this prototype.
    //       If GAS_TASKS length is > a
    //       certain #, all sections fill
    //       w/ fog. If TECH_TASKS length
    //       is > a certain #, the cameras
    //       will malfunction.

    public bool broken;
    public bool fixing;
    public float fixPercent;

    float breaktime;
    float fixtime;

    List<GameObject> cleanUp = new List<GameObject>() { };

    private void Start()
    {
        breaktime = Random.Range(task.BREAK_TIME_MIN, task.BREAK_TIME_MAX);
        fixtime = Random.Range(task.FIX_TIME_MIN, task.FIX_TIME_MAX);
    }

    private void FixedUpdate()
    {
        Credits.theScore -= 
            ((TECH_TASKS.Count * .0001f) + (GAS_TASKS.Count * 0.000025f) * (Time.deltaTime / 4f));
        if (!LevelHandlerTest.gameOver && LevelHandlerTest.gameOn)
        {
            if (!broken)
            {
                if (timer < breaktime)
                {
                    timer += Time.deltaTime;
                }
                else
                {
                    BreakThing();
                    timer = 0f;
                }
            }
            else
            {
                if (fixing)
                {
                    if (timer < fixtime)
                    {
                        timer += Time.deltaTime;
                    }
                    else
                    {
                        FixThing();
                        timer = 0;
                    }
                }
                else
                {
                    //Debug.Log("Not fixing!");
                    if (selectedNPC != null)
                    {
                        //Debug.Log(Vector3.Distance(transform.position, selectedNPC.transform.position));
                        if (Vector3.Distance(transform.position, selectedNPC.transform.position)
                        <= task.NPC_DIST)
                        {
                            //Debug.Log("Fixing!");
                            GameObject c = Instantiate(task.FIXING_FX, 
                                this.transform.position, Quaternion.identity);
                            cleanUp.Add(c);
                            fixing = true;
                        }
                    }
                }
            }
        }
    }


    void BreakThing()
    {
        //Debug.Log("BreakThing()");
        broken = true;

        GameObject a = Instantiate(task.BROKEN_FX, this.transform);
        cleanUp.Add(a);

        switch (task.TASK_TYPE)
        {
            case "TECH":
                TECH_TASKS.Add(this.task);
                //Credits.theScore -= 3.5f * (TECH_TASKS.Count);
                break;
            case "GAS":
                GAS_TASKS.Add(this.task);
                //Credits.theScore -= 1.25f * (GAS_TASKS.Count);
                break;
            default:
                Debug.Log("No such TASK_TYPE");
                break;
        }
        
    }

    public void FixThing()
    {
        //Debug.Log("FixThing()");
        fixing = false;

        breaktime = Random.Range(task.BREAK_TIME_MIN, task.BREAK_TIME_MAX);
        fixtime = Random.Range(task.FIX_TIME_MIN, task.FIX_TIME_MAX);

        foreach (GameObject g in cleanUp)
        {
            Destroy(g);
        }

        selectedNPC = null;
        broken = false;
        LevelHandlerTest.tasksCompleted += 1;

        switch (task.TASK_TYPE)
        {
            case "TECH":
                TECH_TASKS.Remove(this.task);
                Credits.theScore += 3f;
                Credits.theCredits += 20;
                break;
            case "GAS":
                GAS_TASKS.Remove(this.task);
                Credits.theScore += 1f;
                Credits.theCredits += 10;
                break;
            default:
                Debug.Log("No such TASK_TYPE");
                break;
        }
    }
}
