﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeptHealth : MonoBehaviour
{
    // This code is mostly for use only
    //      in the 2nd or 3rd prototype
    //      of Breach of Space. It
    //      should be deleted and
    //      replaced no later than
    //      01/01/2021.            - c.

    public BoxCollider room;// the collider for the room
    public Camera[] cams;   // where ALL cameras go (in case
                            //       there is more than one)
    public float health;
    public float maxHealth;

    public CanvasGroup canvas; // for changing alpha.
    public Slider slidr;       // the ui element.

    public bool notAnExit;     // true when the room is
                               // no longer accessable.
}
