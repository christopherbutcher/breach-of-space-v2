﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Task", menuName = "Tasks")]
public class Task : ScriptableObject
{
    public string TASK_TYPE;

    public float NPC_DIST;

    // Timer for breaking
    public float BREAK_TIME_MIN;
    public float BREAK_TIME_MAX;

    public float FIX_TIME_MIN;
    public float FIX_TIME_MAX;

    // Particles & Effects
    public GameObject BROKEN_FX;   // when broken
    public GameObject FIXING_FX;   // when fixing
    public GameObject FAILURE_FX;  // when failing

    // Audio
    public AudioClip BROKE_LOOP;
    public AudioClip FIX_LOOP;
    public AudioClip FAIL_LOOP;
}
