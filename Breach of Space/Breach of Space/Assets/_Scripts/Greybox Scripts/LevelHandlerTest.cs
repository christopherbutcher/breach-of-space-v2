﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelHandlerTest : MonoBehaviour
{
    public TestClickerV3 camScript;
    public CanvasGroup uiGroup;
    public CanvasGroup results;

    public static bool gameOn;
    public static bool gameOver;

    public static int tasksCompleted;

    // Start is called before the first frame update
    void Start()
    {
        tasksCompleted = 0;
        gameOver = false;
        camScript.CameraShifter(1);
        uiGroup.alpha = 0;
        results.alpha = 0;
        ClockIn();
        gameOn = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        /*if (!gameOn)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                ClockIn();
                gameOn = true;
            }
        }*/

        if (gameOver)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene(0);
            }
        }
    }

    void ClockIn()
    {
        camScript.CameraShifter(0);
        uiGroup.alpha = 1;
    }

    public void ClockOut()
    {
        gameOver = true;
        uiGroup.alpha = 0;
        results.alpha = 1;
        results.GetComponentInChildren<Text>().text =
            "EMPLOYEE EVALUATION\n\n" +
            "TASKS COMPLETED: " + tasksCompleted + "\n" +
            "CREDITS EARNED: " + Credits.theCredits + "\n" +
            "RATING: " + (100 + Credits.theScore) + "\n\n" +
            "PRESS ENTER TO CLOCK BACK IN!";
    }
}
