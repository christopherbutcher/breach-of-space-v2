﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironShifter : MonoBehaviour
{
    List<Camera> camList = new List<Camera>() { };
    List<GameObject> environCleaner = new List<GameObject>() { };
    public Camera camA;
    public Camera camB;

    public Shader gbShade;
    public GameObject smokeFX;

    public bool foggy;
    public bool loPwr;

    void Start()
    {
        camList.Add(camB);
        camList.Add(camA);
    }

    void Update()
    {
        //Debug.Log(TaskmasterTest.TECH_TASKS.Count);
        if (!foggy)
        {
            if(TaskmasterTest.GAS_TASKS.Count >= 5)
            {
                foreach(Camera c in camList)
                {
                    GameObject s = Instantiate(smokeFX, c.transform);
                    environCleaner.Add(s);
                }
                foggy = true;
            }
        }
        else
        {
            if(TaskmasterTest.GAS_TASKS.Count <= 0)
            {
                foreach(GameObject g in environCleaner)
                {
                    Destroy(g);
                }
                foggy = false;
            }
        }

        if (!loPwr)
        {
            if(TaskmasterTest.TECH_TASKS.Count >= 4)
            {
                /*foreach(Camera c in camList)
                {
                    c.RenderWithShader(gbShade, "Untagged");
                }
                loPwr = true;*/
            }
        }

    }
}
