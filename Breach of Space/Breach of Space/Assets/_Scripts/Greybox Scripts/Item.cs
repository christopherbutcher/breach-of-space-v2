﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Item", menuName = "Items")]
public class Item : ScriptableObject
{
    public string ITEM_TYPE;
    public float SPEED_MULT;
    public float WORK_MULT;
}
