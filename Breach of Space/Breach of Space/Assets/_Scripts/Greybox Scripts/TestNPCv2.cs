﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class TestNPCv2 : MonoBehaviour
{
    public bool selected;
    public TaskmasterTest currentTask;

    public NavMeshAgent navAgent;
    public GreyboxNPC NPC_ID;
    public Vector3 destination;

    public Material outlineMat;
    public Material regularMat;

    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (destination != null)
        {
            navAgent.SetDestination(destination);
        }

        if (selected)
        {
            GetComponentInChildren<MeshRenderer>().material = outlineMat;
        }
        else
        {
            GetComponentInChildren<MeshRenderer>().material = regularMat;
        }
    }

    public void WorkinHard(TaskmasterTest t)
    {
        switch (t.task.TASK_TYPE)
        {
            case "GAS":
                navAgent.stoppingDistance = t.task.NPC_DIST - 2f;  // NOT SURE WHY THIS IS HAPPENING
                break;                                             // BUT THE TASKS ARE ALWAYS
                                                                   // OUT OF RANGE NOW???
                                                                   // MAY HAVE SOMETHING TO DO W/
                                                                   // THE NAVMESH.
            default:
                navAgent.stoppingDistance = (t.task.NPC_DIST - 2);
                break;
        }
        // Determines how big the obj. is that the
        //            NPC will interact with. This
        //            way, the NPC will not walk 
        //            into an object when a dest-
        //            ination is set.
        destination = t.gameObject.transform.position;
        selected = false;
    }

    public void HardlyWorkin(Vector3 v)
    {
        Debug.Log("Hardly workin'");
        navAgent.stoppingDistance = 0;
        destination = v;
        selected = false;
    }

    public Vector3 Roaming(float radius)
    {
        Vector3 randomDir = Random.insideUnitSphere * radius;
        randomDir += transform.position;
        NavMeshHit navHit;
        Vector3 finalPos = Vector3.zero;
        if(NavMesh.SamplePosition(randomDir, out navHit, radius, 1))
        {
            finalPos = navHit.position;
        }
        return finalPos;
    }
}
