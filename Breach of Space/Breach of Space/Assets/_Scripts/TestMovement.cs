﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMovement : MonoBehaviour
{
    Vector3 leftSide = new Vector3(-40f, 3.2f, 50.97f);
    Vector3 riteSide = new Vector3(-11f, 3.2f, 50.97f);
    bool goingLeft;

    /// <summary>
    /// Test Code!
    /// </summary>
    
    private void Update()
    {
        if(Vector3.Distance(transform.position, leftSide) > 0 && goingLeft)
        {
            transform.position = Vector3.MoveTowards(transform.position, leftSide, .025f);
        }
        else
        {
            goingLeft = false;
        }

        if (Vector3.Distance(transform.position, riteSide) > 0 && !goingLeft)
        {
            transform.position = Vector3.MoveTowards(transform.position, riteSide, .025f);
        }
        else
        {
            goingLeft = true;
        }
    }
}
