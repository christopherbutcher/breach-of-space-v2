﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Messenger : MonoBehaviour
{
    public GameObject msg_G;
    public GameObject msg_Y;
    public GameObject msg_R;

    public Canvas ws_HOME; // The messages & such must be
    public Canvas ws_COMM; //     children of the Canvases,
    public Canvas ws_ENGI; //     not the CanvasGroups. I
    public Canvas ws_ITEM; //     think? Probably.

    public List<GameObject> MSGS_comms = new List<GameObject>();
    public List<GameObject> MSGS_engine = new List<GameObject>();

    SfxrSynth yah = new SfxrSynth();
    SfxrSynth uhh = new SfxrSynth();
    SfxrSynth ahh = new SfxrSynth();

    private void Start()
    {
        yah.parameters.SetSettingsString(
            "0,.225,.1,.1571,,.0182,.3,.2281,,.44,.11" +
            ",,,,,,,,,,.0717,,,,,1,,,.1,,,");
        uhh.parameters.SetSettingsString(
            "0,.225,.1,.1571,,.0182,.3,.2281,,.23,.11" +
            ",,,,,,,,,,.0717,,,,,1,,,.1,,,");
        ahh.parameters.SetSettingsString(
            "0,.225,.1,.1571,.516,.153,.161,.298,,-.27,.11" +
            ",,,,,,,,,,.0717,,,,,1,,,.1,,.782,.92");
    }

    public void MessageMaker(int severity, string mtext, P4JOBMASTER jobmaster)
    {
        // JOBMASTER = determines which screen to put it on.
        switch (severity)
        {
            case 0: // GREEN
                if(jobmaster.name == "MASTER C")
                {
                    foreach(GameObject m in MSGS_engine)
                    {
                        m.transform.localPosition += new Vector3(0, 41.22f, 0);
                    }
                    GameObject g = Instantiate(msg_G, ws_ENGI.transform, false);
                    g.GetComponent<Message>().WriteMessage(mtext);
                    MSGS_engine.Add(g);
                    if(MSGS_engine.Count > 4)
                    {
                        Destroy(MSGS_engine[0].gameObject);
                        MSGS_engine.RemoveAt(0);
                    }
                }
                else
                {
                    foreach (GameObject m in MSGS_comms)
                    {
                        m.transform.localPosition += new Vector3(0, 41.22f, 0);
                    }
                    GameObject g = Instantiate(msg_G, ws_COMM.transform, false);
                    g.GetComponent<Message>().WriteMessage(mtext);
                    MSGS_comms.Add(g);
                    if (MSGS_comms.Count > 4)
                    {
                        Destroy(MSGS_comms[0].gameObject);
                        MSGS_comms.RemoveAt(0);
                    }
                }
                yah.Play();
                break;
            case 1: // YELLOW
                if (jobmaster.name == "MASTER C")
                {
                    foreach (GameObject m in MSGS_engine)
                    {
                        m.transform.localPosition += new Vector3(0, 41.22f, 0);
                    }
                    GameObject y = Instantiate(msg_Y, ws_ENGI.transform, false);
                    y.GetComponent<Message>().WriteMessage(mtext);
                    MSGS_engine.Add(y);
                    if (MSGS_engine.Count > 4)
                    {
                        Destroy(MSGS_engine[0].gameObject);
                        MSGS_engine.RemoveAt(0);
                    }
                }
                else
                {
                    foreach (GameObject m in MSGS_comms)
                    {
                        //Debug.Log(m.name + ": " + m.transform.position);
                        //Debug.Log(m.name + ": " + m.transform.localPosition);
                        m.transform.localPosition += new Vector3(0, 41.22f, 0);
                        //Debug.Log(m.name + " (after): " + m.transform.position);
                        //Debug.Log(m.name + " (after): " + m.transform.localPosition);
                    }
                    GameObject y = Instantiate(msg_Y, ws_COMM.transform, false);
                    y.GetComponent<Message>().WriteMessage(mtext);
                    MSGS_comms.Add(y);
                    if (MSGS_comms.Count > 4)
                    {
                        Destroy(MSGS_comms[0].gameObject);
                        MSGS_comms.RemoveAt(0);
                    }
                }
                uhh.Play();
                break;
            case 2: // RED
                if (jobmaster.name == "MASTER C")
                {
                    foreach (GameObject m in MSGS_engine)
                    {
                        m.transform.localPosition += new Vector3(0, 41.22f, 0);
                    }
                    GameObject r = Instantiate(msg_R, ws_ENGI.transform, false);
                    r.GetComponent<Message>().WriteMessage(mtext);
                    MSGS_engine.Add(r);
                    if (MSGS_engine.Count > 4)
                    {
                        Destroy(MSGS_engine[0].gameObject);
                        MSGS_engine.RemoveAt(0);
                    }
                }
                else
                {
                    foreach (GameObject m in MSGS_comms)
                    {
                        m.transform.localPosition += new Vector3(0, 41.22f, 0);
                    }
                    GameObject r = Instantiate(msg_R, ws_COMM.transform, false);
                    r.GetComponent<Message>().WriteMessage(mtext);
                    MSGS_comms.Add(r);
                    if (MSGS_comms.Count > 4)
                    {
                        Destroy(MSGS_comms[0].gameObject);
                        MSGS_comms.RemoveAt(0);
                    }
                }
                ahh.Play();
                break;
            default:
                Debug.LogError("Don't shoot the messenger...");
                break;
        }
    }
}
