﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemFixer : MonoBehaviour
{
    // YOU WANTED D.P.V.,
    // YOU GOT D.P.V.!

    public bool isFixable;

    public void FixerUpper()
    {
        if (isFixable)
        {
            GetComponent<MeshCollider>().enabled = true;
            GetComponent<TestStaff4>().isItem = false;
            Debug.Log("D.P.V., baby!");
        }
    }
}
