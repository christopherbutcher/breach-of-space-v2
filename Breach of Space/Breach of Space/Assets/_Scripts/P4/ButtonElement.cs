﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Button Element", menuName = "Button")]
public class ButtonElement : ScriptableObject
{
    public string letter;
    public Sprite keyUp;
    public Sprite keyDown;
}
