﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPreRender : MonoBehaviour
{
    public delegate void PreCullEvent();
    public static PreCullEvent onPreCull;

    void OnPreCull()
    {
        if (onPreCull != null)
        {
            onPreCull();
        }
    }
}
