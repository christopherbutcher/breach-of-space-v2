﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "New Job List", menuName = "List of Job")]
public class P4JOBMASTER : ScriptableObject
{
    public int levelNumber;
    public int taskNumber;
    public float productivity;
    public float startingProductivity;

    public List<Job> quest;

    public List<Job> editableQuest;

    private void OnEnable()
    {
        productivity = startingProductivity;
        editableQuest.Clear();
        foreach(Job j in quest)
        {
            editableQuest.Add(j);
        }
    }
}
