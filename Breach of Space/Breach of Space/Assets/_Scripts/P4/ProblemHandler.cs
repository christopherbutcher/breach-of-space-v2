﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProblemHandler : MonoBehaviour
{
    public ProblemHandler[] allprobs; // all problem handlers in the scene.
    public P4JOBHANDLER thisJob;      // the jobhandler attached to the 
                                      //   box that this probhandler is 
                                      //   attached to.



    public static bool isSomethingBurning;
    public bool thisMachineIsBroken;

    public GameObject fireball;

    float timer;
    float timer_threshold; 

    float broketimer;
    float broketimer_start = 22;
    bool broked;
    bool workingOnIt;

    void Start()
    {
        timer_threshold = Random.Range(10, 22);
        isSomethingBurning = false;
        thisMachineIsBroken = false;
        fireball.SetActive(false);
        timer = 0;
    }

    void Update()
    {
        if(Proto4Handler.gameON && !Proto4Handler.gameover)
        {
            if (!thisMachineIsBroken)
            {
                timer += Time.deltaTime;
            }
            else
            {
                if (broketimer > 0)
                {
                    broketimer -= Time.deltaTime;
                }


                if (thisJob.npc != null && !workingOnIt && !P4JOBHANDLER.working)
                {
                    if (Vector3.Distance(transform.position, thisJob.npc.transform.position) <= 5.25f)
                    {
                        thisJob.ProbSelecter(Random.Range(0, 3));
                        workingOnIt = true;
                    }
                }
            }

            if (timer >= timer_threshold)
            {
                int t = Random.Range(0, 100);
                if (t >= 92)
                {
                    Broken();
                }
                else
                {
                    timer = 0;
                    timer_threshold *= 0.9f;
                }
            }

            if (thisMachineIsBroken && !isSomethingBurning && !P4JOBHANDLER.working)
            {
                isSomethingBurning = true;
            }

            //if (thisMachineIsBroken)
            //{

            //}
        }
    }

    public void Broken()
    {
        broketimer = broketimer_start;
        thisMachineIsBroken = true;
        isSomethingBurning = true;
        fireball.SetActive(true);
        int i = Random.Range(0, 3);
        switch (i)
        {
            case 0:
                thisJob.messenger.MessageMaker(2, this.gameObject.name +
                    "\nis experiencing some\n technical difficulties!", 
                    thisJob.jobMaster);
                break;
            case 1:
                thisJob.messenger.MessageMaker(2, this.gameObject.name +
                    "\nhas some light\nfire damage!", thisJob.jobMaster);
                break;
            case 2:
                thisJob.messenger.MessageMaker(2, this.gameObject.name +
                    "\nrequires urgent\nattention!!", thisJob.jobMaster);
                break;
            default:
                thisJob.messenger.MessageMaker(2, this.gameObject.name +
                    "\nis broken!!!", thisJob.jobMaster);
                break;
        }        
    }

    public void Fixer()
    {
        Proto4Handler.credits += 20;
        timer = 0;
        timer_threshold *= 1.25f;
        fireball.SetActive(false);
        thisMachineIsBroken = false;
        int i = 0;
        foreach (ProblemHandler p in allprobs)
        {
            if (p.thisMachineIsBroken)
            {
                i++;
            }
        }
        if(i <= 0)
        {
            isSomethingBurning = false;
        }

        if(broketimer <= 0)
        {
            if(Proto4Handler.research > 0)
            {
                Proto4Handler.research -= 1;
            }
            broketimer = broketimer_start;
        }
        thisJob.npc = null;
        workingOnIt = false;

    }
}
