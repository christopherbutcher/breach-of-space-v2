﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class TestClickerV4 : MonoBehaviour
{
    public Camera mainCam;
    public Camera aCam;
    public Camera bCam;
    public Camera cCam;
    public Camera compCam;

    public Camera portalCam = null;
    GameObject currentStaff;
    GameObject currentTask;
    public static GameObject currentItem;

    // multiple monitors
    // (there has to be a better way!)
    public GameObject screen4B;
    public GameObject screen4C;

    // bools for Cinemachine
    public bool viewL;
    public bool viewR;
    public bool computing;
    public bool vending;

    SfxrSynth synth = new SfxrSynth();
    SfxrSynth clixsynth = new SfxrSynth();
    SfxrSynth nopeSynth = new SfxrSynth();

    public CanvasGroup spacebar;
    public CanvasGroup shift;

    private void Start()
    {
        synth.parameters.SetSettingsString("0," +
            ".025,,.095,,.099,.461,.2795,,.23" +
            ",,,,,,,,,,,.1482,,,.4,,1,,,.1,,,");
        nopeSynth.parameters.SetSettingsString(
            "0,.0525,,.0069,,.299,.3,.253,,-.25,.02" +
            ",,,,,,,,,,.1224,,,,,1,,,,,,");
        clixsynth.parameters.SetSettingsString("3,.01" +
            ",,,,,.2913,.185,,-.18,.7319,.0397,-.6889" +
            ",,,.3915,-.2497,-.4609,-.6895,.877,-.5119," +
            "-.6194,-.8001,-.0562,.0003,.969,-.0257," +
            ".5202,.7482,-.1809,1,-1");
    }

    void Update()
    {
        GetComponent<Animator>().SetBool("viewL", viewL);
        GetComponent<Animator>().SetBool("viewR", viewR);
        GetComponent<Animator>().SetBool("vending", vending);
        GetComponent<Animator>().SetBool("computing", computing);

        if (Proto4Handler.gameON && !Proto4Handler.screen_up)
        {
            if (Input.GetMouseButtonDown(0))
            {
                //Debug.Log("CLICK");
                clixsynth.Play();
                if (portalCam == null) // && screen is down???
                {
                    //Debug.Log("if (portalCam == null)");
                    RaycastHit theHit;
                    Ray camRay = mainCam.ScreenPointToRay(Input.mousePosition);

                    if (Physics.Raycast(camRay, out theHit))
                    {
                        synth.PlayMutated();
                        //Debug.Log("if (Physics.Raycast(camRay, out theHit))");
                        switch (theHit.collider.name)
                        {
                            case "Screen 4A":
                                if (portalCam != aCam)
                                {
                                    currentStaff = null;
                                    portalCam = aCam;
                                    CameraShifter(2);
                                }
                                break;
                            case "Screen 4B":
                                if (portalCam != bCam)
                                {
                                    currentStaff = null;
                                    portalCam = bCam;
                                    CameraShifter(3);
                                }
                                break;
                            case "Screen 4C":
                                if (portalCam != cCam)
                                {
                                    currentStaff = null;
                                    portalCam = cCam;
                                    CameraShifter(3);
                                }
                                break;
                            case "COMP SCREEN":
                                if (portalCam != compCam)
                                {
                                    currentStaff = null;
                                    portalCam = compCam;
                                    CameraShifter(1);
                                }
                                break;
                            default:
                                portalCam = null;
                                CameraShifter(0);
                                break;
                        }
                    }
                }
                else
                {
                    //Debug.Log("first else");
                    RaycastHit theHit;
                    Ray camRay = mainCam.ScreenPointToRay(Input.mousePosition);

                    if(currentItem == null)
                    {
                        //Debug.Log("if(currentItem == null)");
                        if (Physics.Raycast(camRay, out theHit))
                        {
                            //Debug.Log(theHit.collider.name);
                            if (currentStaff == null)
                            {
                                Ray outRay = portalCam.ViewportPointToRay(theHit.textureCoord);
                                RaycastHit[] xray = Physics.RaycastAll(outRay);

                                foreach (RaycastHit h in xray)
                                {
                                    //Debug.Log(h.collider.gameObject.name);
                                    if (h.collider.gameObject.tag == "NPC")
                                    {
                                        synth.Play();
                                        //Debug.Log("AN NPC!");
                                        currentStaff = h.collider.gameObject;
                                        currentStaff.GetComponent<TestStaff4>().selected = true;
                                        currentStaff.GetComponent<TestStaff4>().circler.SetActive(true);
                                        return;
                                        // stops search
                                    }
                                    else if (h.collider.gameObject.CompareTag("Walls"))
                                    {
                                        //Debug.Log("WALL");
                                        return;
                                        // stops search
                                    }
                                    else
                                    {
                                        //Debug.Log("Nothing!");
                                        // keep searching!
                                    }
                                }

                            }
                            else
                            {
                                Ray outRay = portalCam.ViewportPointToRay(theHit.textureCoord);
                                RaycastHit outHit;

                                if (Physics.Raycast(outRay, out outHit))
                                {
                                    //Debug.Log(outHit.collider.tag);
                                    if (outHit.collider.tag == "NPC")
                                    {
                                        synth.Play();
                                        currentStaff.GetComponent<TestStaff4>().selected = false;
                                        currentStaff.GetComponent<TestStaff4>().circler.SetActive(false);
                                        currentStaff = outHit.collider.gameObject;
                                        currentStaff.GetComponent<TestStaff4>().selected = true;
                                        currentStaff.GetComponent<TestStaff4>().circler.SetActive(true);
                                    }
                                    else if (outHit.collider.tag == "Job")
                                    {
                                        if (!ProblemHandler.isSomethingBurning ||
                                            outHit.collider.GetComponent<ProblemHandler>().thisMachineIsBroken)
                                        {
                                            synth.PlayMutated();
                                            //Debug.Log("HEY, A JOB!");
                                            // task stuff!
                                            outHit.collider.GetComponent<P4JOBHANDLER>().npc =
                                                currentStaff.GetComponent<TestStaff4>();
                                            //outHit.collider.GetComponent<P4JOBHANDLER>().ringer.SetActive(true);
                                            currentStaff.GetComponent<TestStaff4>().
                                                WorkinHard(outHit.collider.GetComponent<P4JOBHANDLER>());
                                            currentStaff = null;
                                        }
                                        else
                                        {
                                            nopeSynth.Play();
                                        }
                                        
                                    }
                                    else if (outHit.collider.tag == "Item")
                                    {
                                        //Debug.Log("THIS IS AN ITEM!");
                                        synth.Play();
                                        // -------------------------------- //
                                        // ITEM CODE GOES HERE, OBVIOUSLY!! //
                                        // -------------------------------- //
                                        outHit.collider.GetComponent<ItemCodeP4>().npc =
                                            currentStaff;
                                        currentStaff.GetComponent<TestStaff4>().HardlyWorkin(
                                            outHit.collider.transform.position);
                                    }
                                    else
                                    {
                                        //Debug.Log("ELSE...?");
                                        // must be a location
                                        currentStaff.GetComponent<TestStaff4>().HardlyWorkin(
                                            outHit.point);
                                        currentStaff = null;
                                    }
                                }
                                else
                                {
                                    //Debug.Log("I did not see anything?");
                                }
                            }
                        }
                    }
                    else
                    {
                        //Debug.Log("GOT ITEM");
                        // you have an item!
                        if (Physics.Raycast(camRay, out theHit))
                        {
                            Ray outRay = portalCam.ViewportPointToRay(theHit.textureCoord);
                            RaycastHit outHit;

                            if (Physics.Raycast(outRay, out outHit))
                            {
                                if (NavMesh.SamplePosition(outHit.point, out NavMeshHit nmh, 1, NavMesh.AllAreas))
                                {
                                    currentItem.GetComponent<NavMeshAgent>().Warp(
                                        new Vector3(outHit.point.x, outHit.point.y, outHit.point.z));
                                    currentItem.GetComponent<ItemFixer>().FixerUpper();
                                    currentItem = null;
                                }
                                else
                                {
                                    Debug.Log("Something isn't working!");
                                }
                            }    
                        }

                    }

                    
                }
            }
            if (Input.GetKeyDown(KeyCode.LeftShift) ||
                Input.GetKeyDown(KeyCode.RightShift))
            {
                if (portalCam != null)
                {
                    switch (portalCam.name)
                    {
                        case "CAM A4":
                            break;
                        case "CAM B4":
                            screen4C.SetActive(true);
                            screen4B.SetActive(false);

                            portalCam = cCam;
                            synth.PlayMutated();
                            break;
                        case "CAM C4":
                            screen4B.SetActive(true);
                            screen4C.SetActive(false);

                            portalCam = bCam;
                            synth.PlayMutated();
                            break;
                        default:
                            Debug.LogError("No such camera.");
                            break;
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                if(portalCam != null)
                {
                    synth.Play();
                }
                portalCam = null;
                CameraShifter(0);
            }
        }
    }

    //public void ItemPlacer(GameObject g, Vector3 vec)
    //{
    //
    //}

    public void CameraShifter(int i)
    {
        //Debug.Log(i);
        switch (i)
        {
            case 0:
                viewR = false;
                viewL = false;
                vending = false;
                computing = false;
                spacebar.alpha = 0;
                shift.alpha = 0;
                break;
            case 1:
                viewR = false;
                viewL = false;
                vending = false;
                computing = true;
                spacebar.alpha = 1;
                shift.alpha = 0;
                break;
            case 2:
                viewR = false;
                viewL = true;
                vending = false;
                computing = false;
                spacebar.alpha = 1;
                shift.alpha = 0;
                break;
            case 3:
                viewR = true;
                viewL = false;
                vending = false;
                computing = false;
                spacebar.alpha = 1;
                shift.alpha = 1;
                break;
            case 4:
                viewR = false;
                viewL = false;
                computing = false;
                vending = true;
                spacebar.alpha = 0;
                shift.alpha = 0;
                break;
            default:
                Debug.LogError("No such camera exists...");
                viewR = false;
                viewL = false;
                vending = false;
                computing = false;
                break;
        }
    }

}