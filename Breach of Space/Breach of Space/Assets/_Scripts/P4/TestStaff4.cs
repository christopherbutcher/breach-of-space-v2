﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class TestStaff4 : MonoBehaviour
{
    public bool selected;

    public TaskmasterTest currentTask;
    public NavMeshAgent navAgent;
    public Vector3 destination;
    public Animator anim;

    public GameObject outlineCircle;
    // Plane under current NPC to
    // show that this char. is
    // currently selected &/or
    // doing something.
    // (need one for tasks aussi)

    public bool walking;
    public bool fixing;
    public bool done;

    public GameObject circler;

    float boredTimer;// timer for boredom.
    float attnSpan;  // max time before bored.

    public float speed;
    public float reg_speed = 3.5f;
    public bool speedUP;
    public float speedTimer;
    float speedTimer_max = 10;

    public bool isItem; // DPV.

    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        circler.SetActive(false);
        destination = transform.position;
        speed = reg_speed;
    }

    void Update()
    {
        anim.SetBool("Walking", walking);
        anim.SetBool("Fixing", fixing);
        anim.SetBool("Done", done);

        if (!isItem) // keeps the robit from breaking the game
                    // because there is no NavMesh under them!
        {
            if (destination != null)
            {
                navAgent.SetDestination(destination);
                if (Vector3.Distance(transform.position, destination) > navAgent.stoppingDistance + 0.25f)
                {
                    walking = true;
                }
                else
                {
                    walking = false;
                    if (!selected)
                    {
                        circler.SetActive(false);
                    }

                }
            }
        }

        if (speedUP)
        {
            //Debug.Log("speed up");
            speedTimer = 0;
            speed = speed * 3;
            navAgent.speed = speed;
            speedUP = false;
        }

        if(speedTimer < speedTimer_max)
        {
            speedTimer += Time.deltaTime;
        }
        else
        {
            if(speed != reg_speed)
            {
                //Debug.Log("speed down");
                speedUP = false;
                speed = reg_speed;
                navAgent.speed = speed;
            }
        }
    }

    public void WorkinHard(P4JOBHANDLER p)
    {
        circler.SetActive(true);
        //navAgent.stoppingDistance = 2f;
        destination = p.workspace.transform.position;
        selected = false;
    }

    public void HardlyWorkin(Vector3 v)
    {
        //Debug.Log("Hardly workin'");
        circler.SetActive(true);
        //navAgent.stoppingDistance = 0;
        destination = v;
        selected = false;
    }

    public Vector3 Roaming(float radius)
    {
        Vector3 randomDir = Random.insideUnitSphere * radius;
        randomDir += transform.position;
        NavMeshHit navHit;
        Vector3 finalPos = Vector3.zero;
        if (NavMesh.SamplePosition(randomDir, out navHit, radius, 1))
        {
            finalPos = navHit.position;
        }
        return finalPos;
    }

}