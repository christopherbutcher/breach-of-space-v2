﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Job", menuName = "Jobs")]
public class Job : ScriptableObject
{
    public string JOB_NAME;
    public string JOB_DESC;
    public string JOB_HOWTO;

    public int MINIGAME_TYPE;

    public bool completed;

    public float JOB_TIME_MIN;
    public float JOB_TIME_MAX;
    // if Job has a time attached.

    public int JOB_PAY_MIN;
    public int JOB_PAY_MAX;
    public string RESOURCE_TYPE;

    public float NPC_DIST;

    public GameObject WORKING_FX;
    // might be null, might not.

    //float cooldown;
    //public float COOLDOWN_MIN;
    //public float COOLDOWN_MAX;

    bool IN_USE;              // if being used
    public bool problemFound; // if broken
    public bool outOfOrder;   // if destroyed

    public int DIFF_MULT;
    // difficulty multiplyer for minigames

    [TextArea]
    public string msg_PRE;
    [TextArea]
    public string msg_POST;
}
