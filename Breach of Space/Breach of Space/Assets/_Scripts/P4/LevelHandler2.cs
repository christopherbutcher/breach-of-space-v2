﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelHandler2 : MonoBehaviour
{
    public TestClickerV4 camScript;
    public CanvasGroup uiGroup;
    public CanvasGroup results; 

    public static bool gameOn;
    public static bool gameOver;

    public static int tasksCompleted;

    public List<P4JOBMASTER> questline = new List<P4JOBMASTER>();
    // OKAY, so:
    //    The entire "questline" is made up of
    //    multistep processes, each one a list
    //    of Jobs. Each Job in each multistep 
    //    process must be completed IN ORDER, 
    //    but the multistep processes themselves 
    //    can be started & completed in any order.
    public List<P4JOBMASTER> editableQuestline = new List<P4JOBMASTER>();

    void Start()
    {
        foreach (P4JOBMASTER p in questline)
        {
            editableQuestline.Add(p);
        }

        tasksCompleted = 0;
        gameOver = false;
        uiGroup.alpha = 0;
        results.alpha = 0;
        gameOn = true;
    }

    void Update()
    {
        if(editableQuestline.Count >= 1)
        {
            for(int i = editableQuestline.Count; i > 0; i--)
            {
                if (editableQuestline[i-1].editableQuest.Count == 0)
                {
                    editableQuestline.RemoveAt(i - 1);
                    //Debug.Log("Removed another quest from the questline!");
                }
            }
        }
        else
        {
            //Debug.Log("NO MORE TASKS!");
        }
        
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();

            // LOGIC TO GET OUT OF MINIGAME
            // SHOULD GO HERE NOW.
        }

        if (gameOver)
        {
            if (Input.GetKey(KeyCode.Return))
            {
                SceneManager.LoadScene(0);
            }
        }
    }

    void ClockIn()
    {
        camScript.CameraShifter(1);
        uiGroup.alpha = 1;
    }

    public void ClockOut()
    {
        gameOver = true;
        uiGroup.alpha = 0;
        results.alpha = 1;
        results.GetComponentInChildren<Text>().text =
            "EMPLOYEE EVALUATION\n\n" +
            "TASKS COMPLETED: " + tasksCompleted + "\n" +
            "CREDITS EARNED: " + Credits.theCredits + "\n" +
            "RATING: " + (100 + Credits.theScore) + "\n\n" +
            "PRESS ENTER TO CLOCK BACK IN!";
    }
}
