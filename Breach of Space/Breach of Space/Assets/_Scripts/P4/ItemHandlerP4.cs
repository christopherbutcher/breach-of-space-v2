﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemHandlerP4 : MonoBehaviour
{
    public int a_price; // placeholders
    public int b_price; // placeholders
    public int c_price; // placeholders

    SfxrSynth synth = new SfxrSynth();
    SfxrSynth synth_sad = new SfxrSynth();

    public static int currentlySelectedItem;
    // 0 = none
    // 1 = soda
    // 2 = robo

    public GameObject can;
    public GameObject robit;
    // other ones go here too!

    public Proto4Handler workSpace;

    public Text item_NAME;
    public Text item_COST;
    public Text item_DESC;

    void Start()
    {
        //GameObject item = Instantiate(can, new Vector3(1, -20f, -8f), Quaternion.identity);
        //TestClickerV4.currentItem = item; // delete these two lines!

        currentlySelectedItem = 0;

        synth.parameters.SetSettingsString("0,.048," +
            ",.0513,.4878,.3369,.3,.7682,,.05,.06,,," +
            ".048,.266,,.5714,.6244,,,.218,,,,,1,,,,,,");
        synth_sad.parameters.SetSettingsString("1,.048," +
            ".153,,,.201,.3,.468,,-.45,-.29,,,,,,,.162," +
            ".11,,,,,,,1,,,,,.323,");
    }

    public void ItemSelection(int i)
    {
        switch (i)
        {
            case 0:
                currentlySelectedItem = 0;
                item_NAME.text = "";
                item_COST.text = "";
                item_DESC.text = "";
                break;
            case 1:
                currentlySelectedItem = 1;
                item_NAME.text = "SPARKPLUG-Brand Sodafuel";
                item_COST.text = a_price.ToString() + " Credits";
                item_DESC.text = "Use to speed up staff members\n" +
                    "for a brief time.\n\nPOUR SOME SUGAR IN YOUR\nGAS TANK WITH\n" +
                    "SPARKPLUG-Brand Sodafuel!!";
                break;
            case 2:
                currentlySelectedItem = 2;
                item_NAME.text = "R.O.B.I.T. Series EX 0.1 BETA";
                item_COST.text = b_price.ToString() + " Research";
                item_DESC.text = "Add another staff member to a room.\n\n" +
                    "Same classic R.O.B.I.T. you\n" +
                    "know and love, but now \n" +
                    "with slightly more speed!";
                break;
            default:
                currentlySelectedItem = 0;
                Debug.LogError("No such item to select.");
                break;
        }
    }

    public void Shop()
    {
        if(TestClickerV4.currentItem == null)
        {
            switch (currentlySelectedItem)
            {
                case 0:
                    Debug.Log("No item selected!");
                    break;
                case 1:
                    if (Proto4Handler.credits >= a_price)
                    {
                        Proto4Handler.credits -= a_price;
                        GameObject item = Instantiate(can, new Vector3(1, -20f, -8f), Quaternion.identity);
                        // this places the item below the map until it is placed.
                        TestClickerV4.currentItem = item;
                        synth.PlayMutated();
                        workSpace.ScreenMover();
                    }
                    else
                    {
                        synth_sad.Play();
                        //Debug.Log("Looks like yr wallet could use some reloading.");
                    }
                    break;
                case 2:
                    if (Proto4Handler.research >= b_price)
                    {
                        Proto4Handler.research -= b_price;
                        GameObject item = Instantiate(robit, new Vector3(1, -20f, -8f), Quaternion.identity);
                        TestClickerV4.currentItem = item;
                        synth.PlayMutated();
                        workSpace.ScreenMover();
                    }
                    else
                    {
                        synth_sad.Play();
                        //Debug.Log("Looks like yr wallet could use some reloading.");
                    }
                    break;
                default:
                    Debug.LogError("Out of stock!");
                    break;
            }
        }  
    }
}
