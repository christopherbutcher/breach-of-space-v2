﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingUI : MonoBehaviour
{
    public CanvasGroup cg;

    void Start()
    {
        cg.alpha = 1;
        //Debug.Log(transform.position);
    }

    void OnEnable()
    {
        CamPreRender.onPreCull += MyPreCull;
    }

    void OnDisable()
    {
        CamPreRender.onPreCull -= MyPreCull;
    }

    void MyPreCull()
    {
        //we want to look back
        Vector3 difference = Camera.current.transform.position - transform.position;
        transform.LookAt(transform.position - difference, Camera.current.transform.up);
    }

    void Update()
    {
        //Debug.Log(transform.position);
        if(cg.alpha > 0)
        {
            cg.alpha -= 0.5f * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, new Vector3(transform.position.x,
                transform.position.y + 20, transform.position.z), 0.025f);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
