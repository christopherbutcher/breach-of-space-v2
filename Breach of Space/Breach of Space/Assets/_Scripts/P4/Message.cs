﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Message : MonoBehaviour
{
    public int stress;
    public Text roboName;
    public Text msgTXT;

    void Start()
    {
        if(stress != 2)
        {
            int a = Random.Range(0, 10);
            int b = Random.Range(0, 10);
            int c = Random.Range(0, 10);
            int d = Random.Range(0, 10);

            roboName.text = "ROBOT #" +
                a.ToString() + b.ToString() +
                c.ToString() + d.ToString();
        }
        else
        {
            roboName.text = "URGENT!!!";
        }
    }

    public void WriteMessage(string t)
    {
        msgTXT.text = t;
    }
}
