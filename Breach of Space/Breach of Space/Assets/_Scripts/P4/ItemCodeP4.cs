﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCodeP4 : MonoBehaviour
{
    /// <summary>
    /// Attach to the item prefabs for Prototype 4.1 - 5.0
    /// </summary>
    public string ITEM_NAME;
    public GameObject npc;

    public Proto4Handler levelHandler;
    public P4JOBHANDLER jobHandler;

    SfxrSynth synth = new SfxrSynth();

    void Start()
    {
        synth.parameters.SetSettingsString(
            "0,.05,,.0133,,.4614,.3,.2246," +
            ",.2123,,,,,,,,,,,.4013,,.4603,,,1,,,,,,");
    }

    void Update()
    {
        if(npc != null)
        {
            Debug.Log(npc.name);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("ontriggerenter - item");
        //Debug.Log(other.name);
        if(other.gameObject.name == npc.name)
        {
            synth.Play();
            //Debug.Log("switch: ");
            switch (ITEM_NAME)
            {
                case "SODA":
                    //Debug.Log("soda works");
                    other.GetComponent<TestStaff4>().speedUP = true;
                    break;
                case "Firmware":
                    break;
                case "Stopwatch":
                    break;
                default:
                    Debug.Log("This item does not exist, probably!");
                    break;
            }
            Destroy(this.gameObject);
        }
    }
}
