﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class P4JOBHANDLER : MonoBehaviour
{
    /// <summary>
    /// This script is attached to every
    ///   object in a scene that the crew
    ///   can interact with.
    /// </summary>

    public TestStaff4 npc;

    public Job[] jobs;
    public List<Job> editableJobList;
    public P4JOBMASTER jobMaster;
    bool nextJob;
    bool inUse;

    // UI for 'minigames'
    public CanvasGroup jobUI;
    public Slider slider;
    public Text descTxt;
    public Image wKey;
    public Image aKey;
    public Image sKey;
    public Image dKey;

    public static bool working;
    public GameObject resourceSprite;

    public static List<string> buttons =
        new List<string> { "W", "A", "S", "D" };

    // minigame variables
    public float simonCount; // not currently in game,
    public float mashTimes;  // but if we wanted to set
    public float holdTime;   // them, they're here.

    public GameObject ringer;
    public GameObject workspace;

    public ProblemHandler probHandler;

    SfxrSynth synth = new SfxrSynth();
    SfxrSynth clickSynth = new SfxrSynth();


    public Messenger messenger;
    float points;

    private void Start()
    {
        synth.parameters.SetSettingsString("4, .04,, " +
            ".105,, .295, .373, .282,, .19,,, .169, " +
            ".476, .121,,,,,, .1497,,, .14, -.02, 1,,, " +
            ".1267,,, -.87");
        clickSynth.parameters.SetSettingsString(
            "3,.169,,.016,,.073,.079,.21,,,,,,,,,,,,,,,,,,1,,,.1,,,");
        ringer.SetActive(false);
        foreach (Job j in jobs)
        {
            editableJobList.Add(j);
        }
    }

    private void Update()
    {
        if(Proto4Handler.gameON && !Proto4Handler.gameover)
        {
            if (!nextJob && jobs.Length > 0 && editableJobList.Count > 0)
            {
                if (editableJobList[0] == jobMaster.editableQuest[0])
                {
                    //Debug.Log("Next task: " + this.gameObject.name + ": " + editableJobList[0].name);
                    nextJob = true;
                    ringer.SetActive(true);
                    messenger.MessageMaker(1, editableJobList[0].msg_PRE, jobMaster);
                    points = 15;
                }
                else
                {
                    nextJob = false;
                }
            }

            if (points > 0)
            {
                points -= Time.deltaTime;
            }

            if (npc != null)
            {
                ringer.SetActive(true);
            }

            //if (Input.GetKey(KeyCode.Space) && jobUI.alpha > 0)
            //{
            //    // used for backing out of job.
            //    StopAllCoroutines();
            //    jobUI.alpha = 0;
            //    ringer.SetActive(false);
            //}

            if (nextJob && npc != null && !inUse && editableJobList.Count >= 1)
            {
                //Debug.Log(name + ": " + Vector3.Distance(transform.position, npc.transform.position)); 
                if (Vector3.Distance(transform.position, npc.transform.position) <= 5.25f)
                {
                    //Debug.Log("NPC in range!");
                    if (editableJobList.Count >= 1)
                    {
                        JobSelecter(editableJobList[0].MINIGAME_TYPE);
                    }
                }
            }
        }

        if (Proto4Handler.gameover)
        {
            StopAllCoroutines();
            jobUI.alpha = 0;
        }
    }

    public void ProbSelecter(int j)
    {
        // FIX THIS. PLEASE.
        //StopAllCoroutines();
        inUse = true;
        switch (j)
        {
            case 0:
                StartCoroutine(ButtonMash(1));
                messenger.MessageMaker(2, this.gameObject.name + 
                    "\nis back online!", jobMaster);
                break;
            case 1:
                StartCoroutine(HoldPlease(1));
                messenger.MessageMaker(2, this.gameObject.name +
                    "\nis operational!", jobMaster);
                break;
            case 2:
                StartCoroutine(SimonSez(1));
                messenger.MessageMaker(2, this.gameObject.name +
                    "\nis up & running!", jobMaster);
                break;
            default:
                Debug.LogError("ProbSelecter on " + this.gameObject.name + " is out of range.");
                break;
        }
    }

    public void JobSelecter(int j)
    {
        if (!ProblemHandler.isSomethingBurning && !working)
        {
            //StopAllCoroutines();    // safety:
                                    // keeps you from
                                    // doing two at the
                                    // same ding dang time.
            inUse = true;
            // switch: Next job in Job[] job#:
            //         0 = button mash
            //         1 = hold please
            //         2 = simon sez
            // These lead to IEnum (below)
            switch (j)
            {
                case 0:
                    StartCoroutine(ButtonMash(0));
                    break;
                case 1:
                    StartCoroutine(HoldPlease(0));
                    break;
                case 2:
                    StartCoroutine(SimonSez(0));
                    break;
                default:
                    Debug.LogError("JobSelecter on " + this.gameObject.name + " is out of range.");
                    break;
            }
        }
    }

    public IEnumerator ButtonMash(int p)
    {
        working = true;
        //Debug.Log("BUTTON MASH START");
        jobUI.alpha = 1;
        slider.value = 0;
        descTxt.text = "Alternate between the two keys";
        string first = buttons[Random.Range(0, buttons.Count)];
        //Debug.Log("1st: " + first);
        KeyCode keyA = (KeyCode)System.Enum.Parse(typeof(KeyCode), first);
        string second = buttons[Random.Range(0, buttons.Count)];
        //Debug.Log("2nd: " + second);
        KeyCode keyB = (KeyCode)System.Enum.Parse(typeof(KeyCode), second);
        if (first == second)
        {
            second = buttons[Random.Range(0, buttons.Count)];
            keyB = (KeyCode)System.Enum.Parse(typeof(KeyCode), second);
        }
        int times = 0;
        int diff = 1;
        if(p != 0)
        {
            diff = 2;
        }
        else
        {
            diff = editableJobList[0].DIFF_MULT;
        }
        while (times < 5 * diff)
        {
            WASD_Presser(false, first);
            while (!Input.GetKey(keyA))
            {
                //Debug.Log("1st while");
                yield return null;
            }
            clickSynth.PlayMutated();
            //Debug.Log("PRESSED");
            // change keys
            slider.value += (100 / (5 * diff) / 2);
            WASD_Presser(false, second);
            while (!Input.GetKey(keyB))
            {
                //Debug.Log("2nd while");
                yield return null;
            }
            clickSynth.PlayMutated();
            //Debug.Log("ALSO PRESSED");
            slider.value += (100 / (5 * diff) / 2);
            // change keys
            times++;
        }
        //Debug.Log("GOODBYE, BUTTON MASH!");
        // SUCCESS MESSAGE & DONE LOGIC GOES HERE
        // YIELD RETURN NEW WAIT FOR SECONDS REALTIME X
        descTxt.text = "Great job!";
        npc = null;
        
        inUse = false;
        synth.Play();
        yield return new WaitForSeconds(1.5f);
        WASD_Presser(true, second);
        jobUI.alpha = 0;
        if(p == 0) // Future Chris: this is to squash a bug
                  //               where, if a Problem calls
                  //               this function, it will
                  //               remove Jobs from the list.
                  //               Maybe there is a better way?
                  //               Fix for all three.
        {
            ringer.SetActive(false);
            nextJob = false;
            if (editableJobList[0].msg_POST.Length > 4)
            {
                messenger.MessageMaker(0, editableJobList[0].msg_POST, jobMaster);
            }
            editableJobList.RemoveAt(0);         // ok
            jobMaster.editableQuest.RemoveAt(0); // ok
            if(points >= 2)
            {
                Proto4Handler.credits += Mathf.RoundToInt(points);
            }
            else
            {
                Proto4Handler.credits++;
            }
            Proto4Handler.research++;
            // create resource UI image (below)
            Instantiate(resourceSprite, this.gameObject.transform.position, Quaternion.identity);
            Proto4Handler.jobs++;
        }
        else
        {
            probHandler.Fixer();
            Proto4Handler.probs++;
        }
        working = false;

        yield return null;
    }

    public IEnumerator HoldPlease(int p)
    {
        working = true;
        jobUI.alpha = 1;
        descTxt.text = "Press & hold the key";
        slider.value = 0;
        string first = buttons[Random.Range(0, buttons.Count)];
        KeyCode keyA = (KeyCode)System.Enum.Parse(typeof(KeyCode), first);
        WASD_Presser(false, first);
        float timer = 0f;
        while (timer < 2f * editableJobList[0].DIFF_MULT)
        {
            while (!Input.GetKey(keyA))
            {
                yield return null;
            }
            timer += Time.deltaTime;
            slider.value = (timer / 2f) * 100f;
            //Debug.Log(timer);
            yield return null;
        }
        // SUCCESS MESSAGE & DONE LOGIC GOES HERE
        // YIELD RETURN NEW WAIT FOR SECONDS REALTIME X
        descTxt.text = "Fantastic!!";
        WASD_Presser(true, first);
        npc = null;
        
        inUse = false;
        synth.Play();
        yield return new WaitForSeconds(1.5f);
        jobUI.alpha = 0;
        
        if (p == 0) // Future Chris: this is to squash a bug
                    //               where, if a Problem calls
                    //               this function, it will
                    //               remove Jobs from the list.
                    //               Maybe there is a better way?
                    //               Fix for all three.
        {
            nextJob = false;
            ringer.SetActive(false);
            if (editableJobList[0].msg_POST.Length > 4)
            {
                messenger.MessageMaker(0, editableJobList[0].msg_POST, jobMaster);
            }
            editableJobList.RemoveAt(0);         // keep
            jobMaster.editableQuest.RemoveAt(0); // keep
            if (points >= 2)
            {
                Proto4Handler.credits += Mathf.RoundToInt(points);
            }
            else
            {
                Proto4Handler.credits++;
            }
            Proto4Handler.research++;
            // create resource UI image (below)
            Instantiate(resourceSprite, this.gameObject.transform.position, Quaternion.identity);
            Proto4Handler.jobs++;
        }
        else
        {
            probHandler.Fixer();
            Proto4Handler.probs++;
        }
        working = false;

        yield return null;
    }

    public IEnumerator SimonSez(int p)
    {
        working = true;
        jobUI.alpha = 1;
        descTxt.text = "Type the keys in order";
        slider.value = 0;
        int times = 0;
        while (times < 1 * editableJobList[0].DIFF_MULT)
        {
            string first = buttons[Random.Range(0, buttons.Count)];
            KeyCode keyA = (KeyCode)System.Enum.Parse(typeof(KeyCode), first);
            WASD_Presser(false, first);
            yield return new WaitForSeconds(.25f);
            WASD_Presser(true, first);
            yield return new WaitForSeconds(0.175f);

            string second = buttons[Random.Range(0, buttons.Count)];
            KeyCode keyB = (KeyCode)System.Enum.Parse(typeof(KeyCode), second);
            WASD_Presser(false, second);
            yield return new WaitForSeconds(.25f);
            WASD_Presser(true, second);
            yield return new WaitForSeconds(0.175f);

            string third = buttons[Random.Range(0, buttons.Count)];
            KeyCode keyC = (KeyCode)System.Enum.Parse(typeof(KeyCode), third);
            WASD_Presser(false, third);
            yield return new WaitForSeconds(.25f);
            WASD_Presser(true, third);
            yield return new WaitForSeconds(0.175f);

            string fourth = buttons[Random.Range(0, buttons.Count)];
            KeyCode keyD = (KeyCode)System.Enum.Parse(typeof(KeyCode), fourth);
            WASD_Presser(false, fourth);
            yield return new WaitForSeconds(.25f);
            WASD_Presser(true, fourth);
            yield return new WaitForSeconds(0.175f);

            string fifth = buttons[Random.Range(0, buttons.Count)];
            KeyCode keyE = (KeyCode)System.Enum.Parse(typeof(KeyCode), fifth);
            WASD_Presser(false, fifth);
            yield return new WaitForSeconds(.25f);
            WASD_Presser(true, fifth);


            while (!Input.GetKeyDown(keyA))
            {
                yield return null;
            }
            clickSynth.PlayMutated();
            WASD_Presser(false, first);
            slider.value += 20f / editableJobList[0].DIFF_MULT;

            while (!Input.GetKeyDown(keyB))
            {
                yield return null;
            }
            clickSynth.PlayMutated();
            WASD_Presser(false, second);
            slider.value += 20f / editableJobList[0].DIFF_MULT;

            while (!Input.GetKeyDown(keyC))
            {
                yield return null;
            }
            clickSynth.PlayMutated();
            WASD_Presser(false, third);
            slider.value += 20f / editableJobList[0].DIFF_MULT;

            while (!Input.GetKeyDown(keyD))
            {
                yield return null;
            }
            clickSynth.PlayMutated();
            WASD_Presser(false, fourth);
            slider.value += 20f / editableJobList[0].DIFF_MULT;

            while (!Input.GetKeyDown(keyE))
            {
                yield return null;
            }
            clickSynth.PlayMutated();
            WASD_Presser(false, fifth);
            slider.value += 20f / editableJobList[0].DIFF_MULT;

            times++;
        }
        descTxt.text = "Very good!";
        npc = null;
        inUse = false;
        synth.Play();
        yield return new WaitForSeconds(1.5f);
        jobUI.alpha = 0;
        // SUCCESS MESSAGE & DONE LOGIC GOES HERE
        // YIELD RETURN NEW WAIT FOR SECONDS REALTIME X
        
        if (p == 0) // Future Chris: this is to squash a bug
                    //               where, if a Problem calls
                    //               this function, it will
                    //               remove Jobs from the list.
                    //               Maybe there is a better way?
                    //               Fix for all three.
        {
            ringer.SetActive(false);
            nextJob = false;
            if (editableJobList[0].msg_POST.Length > 4)
            {
                messenger.MessageMaker(0, editableJobList[0].msg_POST, jobMaster);
            }
            editableJobList.RemoveAt(0);         // These two still need
            jobMaster.editableQuest.RemoveAt(0); // to be here, though.
            if (points >= 2)
            {
                Proto4Handler.credits += Mathf.RoundToInt(points);
            }
            else
            {
                Proto4Handler.credits++;
            }
            Proto4Handler.research += 2;
            // create resource UI image (below)
            Instantiate(resourceSprite, this.gameObject.transform.position, Quaternion.identity);
            Proto4Handler.jobs++;
        }
        else
        {
            probHandler.Fixer();
            Proto4Handler.probs++;
        }
        working = false;

        yield return null;
    }

    public void WASD_Presser(bool on, string key)
    {
        switch (key)
        {
            case "W":
                wKey.enabled = on;
                aKey.enabled = true;
                sKey.enabled = true;
                dKey.enabled = true;
                break;
            case "A":
                wKey.enabled = true;
                aKey.enabled = on;
                sKey.enabled = true;
                dKey.enabled = true;
                break;
            case "S":
                wKey.enabled = true;
                aKey.enabled = true;
                sKey.enabled = on;
                dKey.enabled = true;
                break;
            case "D":
                wKey.enabled = true;
                aKey.enabled = true;
                sKey.enabled = true;
                dKey.enabled = on;
                break;
            default:
                wKey.enabled = true;
                aKey.enabled = true;
                sKey.enabled = true;
                dKey.enabled = true;
                Debug.LogError("No such key has been set up in WASD_Presser");
                break;
        }
    }
}
