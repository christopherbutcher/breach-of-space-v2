﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialHandler : MonoBehaviour
{
    public TestClickerV4 camScript;
    public static int screenNum;
    public Button button;
    public GameObject[] screens;
    public Text greentxt;
    public Text redtxt;

    string greenStr = "";
    string redStr = "";

    public static bool firewatch;

    void Start()
    {
        foreach(GameObject g in screens)
        {
            g.SetActive(false);
        }
        screens[0].SetActive(true);

        greenStr = "- Read your e-mails";
        redStr = "- Read your e-mails";
    }

    void Update()
    {
        greentxt.text = greenStr;
        redtxt.text = redStr;
    }

    public void ScreenChooser()
    {
        screenNum++;
        //Debug.Log("AHHHHHHHHHHHHHH");

        if(screenNum < 3)
        {
            screens[screenNum].SetActive(true);

            if (screenNum > 0)
            {
                screens[screenNum - 1].SetActive(false);
            }
        }
        else
        {
            if(screenNum == 3)
            {
                screens[screenNum].SetActive(true);
                screens[screenNum - 1].SetActive(false);
                Destroy(button);
                ScreenChooser();
            }
            else
            {
                switch (screenNum)
                {
                    case 4:
                        redStr = "- Press spacebar to check other screens." +
                            "\n- Direct a robot to the machine to install crutial updates." +
                            "\n(Do this by clicking on a robot and then clicking on a computer.)";
                        break;
                    case 5:
                        redStr = "- Great! Now, reboot the system to finish the installation.";
                        break;
                    case 6:
                        if (!firewatch)
                        {
                            redStr = "- Debug errors in back-up server.";
                        }
                        else
                        {
                            redStr = "- !!! - Manually reboot the system!";
                        }
                        break;
                    case 7:
                        if (!firewatch)
                        {
                            redStr = "";
                            greenStr = "- Up & running. Congrats!\n- Press ESC to clock out.";
                        }
                        else
                        {
                            redStr = "- !!! - Manually reboot the system!";
                        }
                        break;
                    case 8:
                        redStr = "";
                        greenStr = "- Up & running. Congrats!\n- Press ESC to clock out.";
                        break;
                    default:
                        break;
                }
                greenStr += "\n" + redStr;
            }
            
            
        }
    }
}
