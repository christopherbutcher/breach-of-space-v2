﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerV2 : MonoBehaviour
{
    public float timeStart;
    public Text Watch;
    public LevelHandler2 LHT;

    void Start()
    {
        //time start value
        Watch.text = "2:00";
        //Watch.text = timeStart.ToString("F2");
    }

    void Update()
    {
        if (!LevelHandler2.gameOver && LevelHandler2.gameOn)
        {
            //adds time to stop watch when playing game
            timeStart += Time.deltaTime;
            float timeLeft = 300f - timeStart;
            Watch.text = FormatTime(timeLeft); //.ToString("F2");

            if (timeStart >= 300)
            {
                Outtatime();
            }
        }
    }

    public string FormatTime(float time)
    {
        int minutes = (int)time / 60;
        int seconds = (int)time - 60 * minutes;
        int milliseconds = (int)(1000 * (time - minutes * 60 - seconds));
        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    void Outtatime()
    {
        LevelHandler2.gameOn = false;
        LHT.ClockOut();
    }
}
