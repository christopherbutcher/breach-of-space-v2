﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Proto4Handler : MonoBehaviour
{
    public static bool gameON;
    public AudioSource song;
    public static bool screen_up;
    public float countdown = 121;
    public Text timerTxt;

    public CanvasGroup resultsScreen;    // end screen
    public Text resultsTXT;

    public CanvasGroup screen_item;
    public CanvasGroup screen_home;
    public CanvasGroup screen_comm;
    public CanvasGroup screen_engi;

    public GameObject workSpace;         // computer (all)

    public GameObject allButtons;
    public GameObject item_ROBO; // might be able to do
    public GameObject item_CAN;  // it without all these,
    public Button button_ROBO;   // but just in case they
    public Button button_CAN;    // are here! - c.
    public Button button_BUY;

    //public Queue<IEnumerator> q_uest; // for handling multiple Coroutines.

    public static int credits;
    public static int research;

    public Text credTXT;
    public Text researchTXT;

    public float hp_comms;
    public float hp_engine;
    public float hp_total;

    public static int jobs;
    public static int probs;

    public TestClickerV4 camScript;

    SfxrSynth upSynth = new SfxrSynth();
    SfxrSynth downSynth = new SfxrSynth();

    Vector3 upV3 = new Vector3(0,0,1.8f);
    Vector3 downV3 = new Vector3(0, -2f, 1.8f);

    public static int tasksCompleted;
    public static bool gameover;

    public List<P4JOBMASTER> questline = new List<P4JOBMASTER>();
    public List<P4JOBMASTER> editableQuestline = new List<P4JOBMASTER>();

    //public AudioClip bto;

    void Start()
    {
        gameON = false;
        gameover = false;
        //credits = 50;   // I'M HACKING
        //research = 50; // THE MAINFRAME
        foreach (P4JOBMASTER p in questline)
        {
            editableQuestline.Add(p);
        }

        upSynth.parameters.SetSettingsString(
            "0,.1,,.2802,,.1102,.3,.3042,,.1954,,,,,,,,,,,.4117,,,,,.8898,,,.0795,,,"); 
        downSynth.parameters.SetSettingsString(
            "0,.1,,.2802,,.1102,.3,.3042,,-.23,,,,,,,,,,,.4117,,,,,.8898,,,.0795,,,"); 

        camScript.CameraShifter(0);
        WhichScreen(1);
        screen_up = true;
        resultsScreen.alpha = 0;
    }

    void Update()
    {
        credTXT.text = credits.ToString() + " Credits";
        researchTXT.text = research.ToString();

        if (gameON)
        {
            if (!song.isPlaying)
            {
                //song.Stop();
                song.Play();
            }

            if (countdown > 0)
            {
                countdown -= Time.deltaTime;
            }
            else
            {
                resultsScreen.GetComponent<CanvasGroup>().alpha = 1;
                screen_up = true;
                StartCoroutine(ShiftScreen(1));
                WhichScreen(5);
                gameover = true;
                gameON = false;
            }

            if (editableQuestline.Count >= 1)
            {
                for (int i = editableQuestline.Count; i > 0; i--)
                {
                    if (editableQuestline[i - 1].editableQuest.Count == 0)
                    {
                        editableQuestline.RemoveAt(i - 1);
                        //Debug.Log("Removed another quest from the questline!");
                    }
                }
            }
            else
            {
                //Debug.Log("NO MORE TASKS!");
                resultsScreen.GetComponent<CanvasGroup>().alpha = 1;
                screen_up = true;
                StartCoroutine(ShiftScreen(1));
                WhichScreen(5);
                gameON = false;
                gameover = true;
            }
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();

            // LOGIC TO GET OUT OF MINIGAME
            // SHOULD GO HERE NOW.
        }

        if (gameover)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                gameON = false;
                research = 0;
                credits = 0;
                gameover = false;
                //Debug.Log("RESTARTING!");
                SceneManager.LoadScene(0);
            }
        }

        timerTxt.text = FormatTime(countdown);

        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (!gameON)
            {
                gameON = true;
                if (!gameover)
                {
                    //Debug.Log("Playing!");
                    song.Play();
                }
            }
            //synth.PlayMutated();
            ScreenMover();
            // bring up / put down the screen!!!!!!!!!
        }
    }

    public string FormatTime(float time)
    {
        int minutes = (int)time / 60;
        int seconds = (int)time - 60 * minutes;
        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    public void ScreenMover()
    {
        //Debug.Log("WORLD: " + workSpace.transform.position);
        //Debug.Log("LOCAL: " + workSpace.transform.localPosition);
        if (screen_up)
        {
            screen_up = false;
            downSynth.Play();
            StartCoroutine(ShiftScreen(0));
            WhichScreen(0);
        }
        else
        {
            if (!P4JOBHANDLER.working)
            {
                screen_up = true;
                upSynth.Play();
                StartCoroutine(ShiftScreen(1));
                WhichScreen(1);
            }
        }
    }

    IEnumerator ShiftScreen(int x)
    {
        switch (x)
        {
            case 0:
                while(Vector3.Distance(workSpace.transform.localPosition, downV3) > 0)
                {
                    workSpace.transform.localPosition = 
                        Vector3.MoveTowards(workSpace.transform.localPosition, downV3, 0.5f);
                    yield return null;
                }
                workSpace.transform.localPosition = downV3; // fixes a bug!
                break;
            case 1:
                while (Vector3.Distance(workSpace.transform.localPosition, upV3) > 0)
                {
                    workSpace.transform.localPosition = 
                        Vector3.MoveTowards(workSpace.transform.localPosition, upV3, 0.5f);
                    yield return null;
                }
                workSpace.transform.localPosition = upV3; // fixes a bug!
                camScript.portalCam = null;
                camScript.CameraShifter(0);
                break;
            default:
                break;
        }
    }

    public void WhichScreen(int i)
    {
        switch (i)
        {
            case 0: // none
                allButtons.SetActive(false);
                item_ROBO.SetActive(false);
                item_CAN.SetActive(false);
                
                screen_home.alpha = 0;
                screen_item.alpha = 0;
                screen_engi.alpha = 0;
                screen_comm.alpha = 0;
                break;
            case 1: // home
                allButtons.SetActive(true);
                button_ROBO.gameObject.SetActive(false);
                button_CAN.gameObject.SetActive(false);
                button_BUY.gameObject.SetActive(false);
                item_ROBO.SetActive(false);
                item_CAN.SetActive(false);

                screen_home.alpha = 1;
                screen_item.alpha = 0;
                screen_engi.alpha = 0;
                screen_comm.alpha = 0;
                break;
            case 2: // comms
                allButtons.SetActive(true);
                button_ROBO.gameObject.SetActive(false);
                button_CAN.gameObject.SetActive(false);
                button_BUY.gameObject.SetActive(false);
                item_ROBO.SetActive(false);
                item_CAN.SetActive(false);

                screen_home.alpha = 0;
                screen_item.alpha = 0;
                screen_engi.alpha = 0;
                screen_comm.alpha = 1;
                break;
            case 3: // engine
                allButtons.SetActive(true);
                button_ROBO.gameObject.SetActive(false);
                button_CAN.gameObject.SetActive(false);
                button_BUY.gameObject.SetActive(false);
                item_ROBO.SetActive(false);
                item_CAN.SetActive(false);

                screen_home.alpha = 0;
                screen_item.alpha = 0;
                screen_engi.alpha = 1;
                screen_comm.alpha = 0;
                break;
            case 4: // items
                ItemHandlerP4.currentlySelectedItem = 0;
                allButtons.SetActive(true);
                button_ROBO.gameObject.SetActive(true);
                button_CAN.gameObject.SetActive(true);
                button_BUY.gameObject.SetActive(true);
                item_ROBO.SetActive(true);
                item_CAN.SetActive(true);

                screen_home.alpha = 0;
                screen_item.alpha = 1;
                screen_engi.alpha = 0;
                screen_comm.alpha = 0;
                break;
            case 5: // results!
                allButtons.SetActive(false);
                item_ROBO.SetActive(false);
                item_CAN.SetActive(false);
                screen_home.alpha = 0;
                screen_item.alpha = 0;
                screen_engi.alpha = 0;
                screen_comm.alpha = 0;
                resultsScreen.alpha = 1;
                resultsTXT.text = 
                    "SHIFT COMPLETED.\n\n" +
                    "Total Jobs Finished: " + jobs + "\n" +
                    "Total Problems Squashed: " + probs + "\n" +
                    "Credits Earned: " + credits + "\n" +
                    "Research Gained: " + research + "\n\n" +
                    "PRESS [ESC] TO QUIT, OR\n" +
                    "PRESS [ENTER] TO PLAY AGAIN!";
                break;
            default:
                Debug.LogError("MISSINGNO.");
                break;
        }
    }
}
