﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPneumatic : MonoBehaviour
{
    public bool selected;

    private void OnCollisionEnter(Collision collision)
    {
        if(selected && collision.collider.tag.Contains("NPC"))
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (selected && other.gameObject.tag.Contains("NPC"))
        {
            Destroy(this.gameObject);
        }
    }
}
