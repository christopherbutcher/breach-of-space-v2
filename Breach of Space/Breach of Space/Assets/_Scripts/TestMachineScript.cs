﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMachineScript : MonoBehaviour
{
    CapsuleCollider capCol;
    GameObject worker;
    bool inRange;
    bool busted;
    bool currentlyWorking;
    float timer;

    public Material yayMat;
    public Material mehMat;
    public Material ahhMat;

    private void Start()
    {
        capCol = GetComponent<CapsuleCollider>();
        currentlyWorking = true;
    }

    void Update()
    {
        if (!busted)
        {
            StartCoroutine(PlannedObsolescence());
        }
    }

    IEnumerator PlannedObsolescence()
    {
        Debug.Log("Planned Obsolescence");
        busted = true;
        yield return new WaitForSeconds(5f);
        int roll = Random.Range(0, 20);
        if (roll <= 5)
        {
            GetComponent<MeshRenderer>().material = ahhMat;
            currentlyWorking = false;
            yield return null;
        }
        busted = false;
        yield return null;
    }

    IEnumerator Fixerupper()
    {
        Debug.Log("Fixer");
        if (inRange 
            /*&& busted && (worker.GetComponent<TestNPC>().working == true) &&*/)
        {
            Debug.Log("Fixer (if)");
            //worker.GetComponent<TestNPC>().moving = false;
            for(int i = 0; i < 3; i++)
            {
                Debug.Log("Fixer (for)");
                GetComponent<MeshRenderer>().material = mehMat;
                yield return new WaitForSecondsRealtime(.5f);
                GetComponent<MeshRenderer>().material = null;
                yield return new WaitForSecondsRealtime(.5f);
            }
            GetComponent<MeshRenderer>().material = yayMat;
            busted = false;
            currentlyWorking = true;
            capCol.radius = .5f;

        }
        yield return null;
    }

    public void MakeTheDonuts(GameObject npc)
    {
        capCol.radius = 1.25f;
        Debug.Log("Making donuts");
        worker = npc;
        Debug.Log(worker);
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Contains("NPC"))
        {
            GetComponent<Light>().range = 0;
            inRange = true;
            if (!currentlyWorking)
            {
                StartCoroutine(Fixerupper());
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Contains("NPC"))
        {
            inRange = false;
            StopCoroutine(Fixerupper());
            if (!currentlyWorking)
            {
                GetComponent<MeshRenderer>().material = ahhMat;
            }
        }
    }
    /*
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger enter (test machine)");
        if (other.gameObject == worker || other.gameObject.tag == "NPC")
        {
            Debug.Log("Trigger enter (test machine) - IF 1");
            inRange = true;
            if (busted)
            {
                Debug.Log("Trigger enter (test machine) - IF 2");
                other.GetComponent<TestNPC>().working = true;
                StartCoroutine(Fixerupper());
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("Trigger exit (test machine)");
        if (other.gameObject == worker)
        {
            inRange = false;
            if (busted)
            {
                other.GetComponent<TestNPC>().working = false;
                StopCoroutine(Fixerupper());
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(Fixerupper());
        /*
        Debug.Log("coll enter (test machine)");
        if (collision.collider.gameObject == worker 
            || collision.collider.gameObject.tag.Contains("NPC"))
        {
            
        }
        Debug.Log("coll enter (test machine) - IF 1");
        inRange = true;
        if (busted)
        {
            Debug.Log("coll enter (test machine) - IF 2");
            collision.collider.GetComponent<TestNPC>().working = true;
            
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.gameObject == worker)
        {
            inRange = false;
            if (busted)
            {
                collision.collider.GetComponent<TestNPC>().working = false;
                StopCoroutine(Fixerupper());
            }
        }
    }*/
}
