﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using UnityEngine;

public class TestClicker : MonoBehaviour
{
    public Camera mainCam;
    public Camera aCam;
    public Camera bCam;
    public Camera cCam;
    public Camera dCam;

    public GameObject ball;


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit theHit;
            Ray camRay = mainCam.ScreenPointToRay(Input.mousePosition);

            if(Physics.Raycast(camRay, out theHit))
            {
                Camera portalCam = null;

                switch (theHit.collider.name)
                {
                    case "Screen A":
                        portalCam = aCam;
                        break;
                    case "Screen B":
                        portalCam = bCam;
                        break;
                    case "Screen C":
                        portalCam = cCam;
                        break;
                    case "Screen D":
                        portalCam = dCam;
                        break;
                    default:
                        portalCam = null;
                        break;
                }
                if(portalCam != null)
                {
                    Ray outRay = portalCam.ViewportPointToRay(theHit.textureCoord);
                    RaycastHit outHit;

                    if (Physics.Raycast(outRay, out outHit))
                    {
                        //Debug.Log(outHit.point);
                        Debug.Log(outHit.collider.name);
                        Instantiate(ball, outHit.point, Quaternion.identity);
                    }
                } 
            }
        }
    }
}
