﻿// using System;
// using System.Collections;
// using System.Collections.Generic;
// using UnityEngine;

// public class GameEvents : MonoBehaviour
// {
   // public static GameEvents current; 

  // private void Awake()
  // {
   //    current = this;
 //  }

 //  public event Action onSomethingTriggerEnter;
  // public void SomethingTriggerEnter()
 //  {
    //   if (onSomethingTriggerEnter != null)
   //    {
   //        onSomethingTriggerEnter();
   //    }
  // }
   
  // public event Action onSomethingTriggerExit;
  // public void SomethingTriggerExit()
  // {
    //   if (onSomethingTriggerExit != null)
   //    {
   //        onSomethingTriggerExit(); 
   //    }
 //  }
//}

// Main script for the event; It is used to trigger the event and connects to the other scripts that is used for the event to happen 
// Skeleton of an custom event system; will have to create seperate scripts for each event 
// but copy and paste this code for each event 
// This script goes with the Trigger Area (triggers event and the object that needs to be trigged 
// I created this script to open doors if you want to see how it works 