﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public AudioSource audios;
    public CanvasGroup fader;

    // Start is called before the first frame update
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void Update()
    {
        
        if (Input.GetKey(KeyCode.Return))
        {
            StartCoroutine(FadeOut());
            // start fade in.
        }
    }

    IEnumerator FadeOut()
    {
        for(int i = 0; i < 10; i++)
        {
            fader.alpha += 0.1f;
            if(audios.volume > 0)
            {
                audios.volume -= 0.05f;
            }
            yield return new WaitForSeconds(0.1f);
        }
        SceneManager.LoadScene(1);
        yield return null;
    }
}
