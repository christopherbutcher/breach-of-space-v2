﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamMover : MonoBehaviour
{
    public bool turned;

    //float turn = -70f;
    float ups = -5f;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (turned)
            {
                transform.position = new Vector3(0, 1, -38.84f);
                transform.Rotate(0, 70, 0);
                turned = false;
            }
            else
            {
                transform.position = new Vector3(ups, 1, -38.84f);
                transform.Rotate(0, -70f, 0);
                turned = true;
            }
        }
    }

    IEnumerator DistractedBoyfriendMemeSimulator()
    {
        if (turned)
        {
            while (transform.rotation.y < 70)
            {
                transform.Rotate(0, 1, 0);
                yield return new WaitForEndOfFrame();
            }
            turned = false;
        }
        else
        {
            while (transform.rotation.y > -70f)
            {
                transform.Rotate(0, -1, 0);
                yield return new WaitForEndOfFrame();
            }
            turned = true;
        }
        yield return null;
    }




}
