﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class TestClickerVers2 : MonoBehaviour
{
    public Camera mainCam;
    public Camera aCam;
    public Camera bCam;
    public GameObject lite;

    public GameObject gift;
    public Transform shopA;
    public Transform shopB;

    Camera portalCam = null;

    TestNPC currentNPC = null;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit theHit;
            Ray camRay = mainCam.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(camRay, out theHit))
            {

                switch (theHit.collider.name)
                {
                    case "Screen 2a":
                        if(portalCam != aCam)
                        {
                            currentNPC = null;
                            portalCam = aCam;
                        }
                        break;
                    case "Screen 2b":
                        if (portalCam != bCam)
                        {
                            currentNPC = null;
                            portalCam = bCam;
                        }
                        break;
                    case "Gift":
                        portalCam = null;
                        currentNPC = null;
                        ReturnOfTheMagi();
                        break;
                    default:
                        portalCam = null;
                        break;
                }
                if (portalCam != null)
                {
                    Ray outRay = portalCam.ViewportPointToRay(theHit.textureCoord);
                    RaycastHit outHit;

                    if (Physics.Raycast(outRay, out outHit))
                    {
                        //Debug.Log(outHit.collider.name);
                        if(outHit.collider.tag == "NPC")
                        {
                            currentNPC = outHit.collider.GetComponent<TestNPC>();
                            currentNPC.selected = true;
                            
                        }
                        else if(
                            ((outHit.collider.gameObject.tag == "TestMachine") ||
                            (outHit.collider.gameObject.tag == "Pneumatic"))
                            && currentNPC != null)
                        {
                            Debug.Log("IT IS AN OBJECT!");
                            currentNPC.JobAssignment(outHit.collider.gameObject, 
                                outHit.collider.transform.position.x,
                                outHit.collider.transform.position.z);
                            currentNPC = null;
                        }
                        else
                        {
                            if(currentNPC != null)
                            {
                                currentNPC.JobAssignment(null, outHit.point.x, outHit.point.z);
                                currentNPC = null;
                            }
                        }
                    }
                }
            }
        }
    }

    void ReturnOfTheMagi()
    {
        Instantiate(gift, shopA.position, Random.rotation);
        Instantiate(gift, shopB.position, Random.rotation);
    }
}
