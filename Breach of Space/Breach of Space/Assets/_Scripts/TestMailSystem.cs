﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMailSystem : MonoBehaviour
{
    public GameObject toob;

    private void Start()
    {
        StartCoroutine(SpecialDelivery());
    }

    IEnumerator SpecialDelivery()
    {
        float r = Random.Range(0f, 5f);
        yield return new WaitForSecondsRealtime(r);
        Instantiate(toob, new Vector3(transform.position.x, 
            transform.position.y, transform.position.z),
            Random.rotation);
        StopAllCoroutines();
        StartCoroutine(SpecialDelivery());
        yield return null;
    }
}
