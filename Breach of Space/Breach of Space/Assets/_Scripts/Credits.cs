﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour
 {
    public Text creditsText;
    public Text scoreText;
    public static int theCredits;
    public static float theScore;

    public LevelHandlerTest LHT;

    private void Update()
    {
        if (!LevelHandlerTest.gameOver && LevelHandlerTest.gameOn)
        {
            creditsText.text = theCredits + " credits";
            scoreText.text = "Evaluation Score: " + (80f + theScore).ToString("F2") + "%";

            if (theScore <= -100f)
            {
                Endgame();
            }

            if(theScore > 100)
            {
                theScore = 100;
            }
        }   
    }

    void Endgame()
    {
        // I am inevitable
        LevelHandlerTest.gameOn = false;
        LHT.ClockOut();
    }

    /*void OnTriggerEnter (Collider other)
    {
        theCredits += 50;
        creditsText.GetComponent<Text>().text = "Credits:" + theCredits;
        Destroy(gameObject);
    }*/
}
